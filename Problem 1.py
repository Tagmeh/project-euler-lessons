def try1():
    total = 0
    for number in range(1000):
        if number % 5 == 0 or number % 3 == 0:
            total += number
    print(total)


def try2():
    total = sum([number for number in range(1000) if number%5==0 or number%3==0])
    print(total)


if __name__ == '__main__':
    try1()
    try2()


# Two different attempts, to see which was faster (previously had timeit wrapping the functions)	
# Answer is 233168
