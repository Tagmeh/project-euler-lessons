triangle_string = '''75
95 64
17 47 82
18 35 87 10
20 04 82 47 65
19 01 23 75 03 34
88 02 77 73 07 63 67
99 65 04 28 06 16 70 92
41 41 26 56 83 40 80 70 33
41 48 72 33 47 32 37 16 94 29
53 71 44 65 25 43 91 52 97 51 14
70 11 33 28 77 73 17 78 39 68 17 57
91 71 52 38 17 14 91 43 58 50 27 29 48
63 66 04 68 89 53 67 30 73 16 69 87 40 31
04 62 98 27 23 09 70 98 73 93 38 53 60 04 23
'''

triangle_list = []
# Convert the string triangle into a list of lists.
for row in triangle_string.splitlines():
    triangle_list.append([int(num) for num in row.split(' ')])

##print(triangle_list)

num_list = []
num_list.append(int(triangle_list[0][0])) # first digit
for row in triangle_list:
    if len(row) == 1:
        prev_index = 0 # First prev_index will always be 0 since it's a triangle and starts at the top.
        continue # Skip first item
    row_index = triangle_list.index(row)
    prev_num = num_list[-1]
##    prev_index = triangle_list[int(row_index)-1].index(int(prev_num))
    highest_num = 0
    for mod in [0, 1]: # checking to the left and right of the index in the row above. Would be -1, 0, 1 if the columns lined up.
        try:
            curr_num = triangle_list[row_index][prev_index + mod]
            
            if curr_num > highest_num:
                highest_num = curr_num
                prev_index = prev_index + mod # Solves the problem of getting the index of a value of which there are multiple same values in the row. Sneaky Project Euler...
        except IndexError:
            pass

    print(row_index, prev_num, prev_index, prev_index + mod)
    num_list.append(highest_num)

print(num_list)
print(sum(num_list))
        
    
    
