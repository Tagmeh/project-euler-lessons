


for a in range(1, 1001): # Get a value for A
	for b in range(1, 1001-a): # Get a value for B
		c = 1000 - a - b   # Solving for C with the guessed values of A and B
		if a*a + b*b == c*c: # if this is true, then the 3 guessed variables are a Pythagorean triplet (for 1000).
			print(a, b, c)
			print(a*b*c) # Requirement of the lesson.

# Answer is 31875000
