num = 999

def pal(num):
    number = num
    highest = 0
    for i in range(number+1):
        for i in reversed(range(number+1)):
            a = i * num
            if str(a) == str(a)[::-1]:
                if a > highest:
                    highest = a

            if i < 100:
                break
            
        num = num - 1
    print(highest)
            
# Answer is 906609
            

