
def factorial(x):
    t = 1
    for i in reversed(range(1, x+1)):
        t = t * i
    return t

def get_lattice_path(a, b):
    c = a + b

    # Get top variable
    n = factorial(c)
    print('n', n)

    # Get first bottom variable
    r = factorial(a)
    print('r', r)

    # Get second bottom variable
    z = factorial(c-a)
    print('z', z)


    bottom = r * z
    total = n // bottom
    print(f'{n} divided by {bottom}')
    print(total)

# Answer is 137846528820
