
# Function found at. (wrote it to try and understand it)
# https://www.geeksforgeeks.org/python-program-to-check-whether-a-number-is-prime-or-not/
# Still not sure what exactly it means.
def is_prime(n):
    if n <= 1: # 1 is not considered a prime.
        return False
    if n <= 3: # 2 and 3 are both primes.
        return True

    # Divides n by the first two prime numbers
    # Because most numbers can be divided by 2 or 3.
    # Weeds out the most numbers.
    if n%2==0 or n%3==0:
        return False

    i = 5 # Starts at prime number 5
    while i*i<=n:
        if n%i==0 or n%(i+2)==0: # Checks against 5 and 7
            # Then checks against 11 and 13, up to the sq root of n (7*7=49, no higher primes than 7 make up 49)
            return False
        i=i+6 # Used to get the next set of primes

    return True

num = 1
prime_list = []
while num < 2000000:
    if is_prime(num):
        prime_list.append(num)
##        print(len(prime_list), num)

    num += 1

print(sum(prime_list))


# Answer is 142913828922
