total = 0

# It took hours to discover that 40 was spelled "forty" and not "fourty"...
# Leaving in testing code and past iterations.

num_dict = {
    1 : 3,  # one
    2 : 3,  # two
    3 : 5,  # three
    4 : 4,  # four
    5 : 4,  # five
    6 : 3,  # six
    7 : 5,  # seven
    8 : 5,  # eight
    9 : 4,  # nine
    10 : 3, # ten
    11 : 6, # eleven
    12 : 6, # twelve
    13 : 8, # thirteen
    14 : 8, # fourteen
    15 : 7, # fifteen
    16 : 7, # sixteen
    17 : 9, # seventeen
    18 : 8, # eighteen 
    19 : 8, # nineteen
    20 : 6, # twenty
    30 : 6, # thirty
    40 : 5, # forty
    50 : 5, # fifty
    60 : 5, # sixty
    70 : 7, # seventy
    80 : 6, # eighty
    90 : 6  # ninety
    }


def count(x):
    total = 0
    for num in range(1, x+1):
        current = 0
        print(str(num) + ' ', end="")

        if num > 999 and num % 1000 == 0:
            total += num_dict[int(str(num)[:1])]
            total += 8 # thousand
            
            current += num_dict[int(str(num)[:1])] + 8
##            print('one thousand', end='')

            num = int(str(num)[1:])
            if num == 0:
                print(current)
                continue
            

        # checks for centenials, strips off first number
        if num % 100 == 0:
            total += 7 # hundred
            total += num_dict[int(str(num)[0])]
            
            current += num_dict[int(str(num)[0])] + 7
##            print('hundreds', end='')

        # checks for 100+, not including centenials (to add the "and" as in "one hundred and three")
        if num > 99 and num % 100 != 0:
            total += num_dict[int(str(num)[0])]
            total += 10 # hundred and
            
            current += num_dict[int(str(num)[0])] + 10
##            print('X hundred and ', end='')
            
            num = int(str(num)[1:])
            if num == 0:
                print(current)
                continue
               
        # numbers below 20
        if num < 20 and num != 10:
            total += num_dict[num]
            
            current += num_dict[num]
##            print('1-19', end='')

        # 10, 20, 30, 40, 50, etc
        if num < 100 and num % 10 == 0:
            total += num_dict[num]
            
            current += num_dict[num]
##            print('tens', end='')

        if num > 20 and num < 100 and num % 10 != 0:
            total += num_dict[num-int(str(num)[-1])]
            total += num_dict[int(str(num)[-1])]
            
            current += num_dict[num-int(str(num)[-1])] 
            current += num_dict[int(str(num)[-1])]
##            print('>20, not tens', end='')

        print(current)

##        if num > 50 and num < 70 and num != 60:
##            total += 5 # fifty, sixty
##            total += num_dict[int(str(num)[-1])]
##            current += 5 # fifty, sixty
##            current += num_dict[int(str(num)[-1])]
##            print(num, current, '50-70')
##
##        if num > 20 and num < 30:
##            total += num_dict[num-int(str(num)[-1])] # twenty
##            total += num_dict[int(str(num)[-1])]
##            current +=6 # twenty
##            current += num_dict[int(str(num)[-1])]
##            print(num, current, '20-30')
##
##        if num > 30 and num < 50 and num != 40:
##            total += 6 # thirty, fourty, 
##            total += num_dict[int(str(num)[-1])]
##            current += 6 # thirty, fourty, 
##            current += num_dict[int(str(num)[-1])]
##            print(num, current, '30-50')
##
##        if num > 80 and num < 100 and num != 90:
##            total += 6 # eighty, ninety
##            total += num_dict[int(str(num)[-1])]
##            current += 6 # eighty, ninety
##            current += num_dict[int(str(num)[-1])]
##            print(num, current, '80-100')
##
##        if num > 70 and num < 80:
##            total += 7 # seventy
##            total += num_dict[int(str(num)[-1])]
##            current += 7 # seventy
##            current += num_dict[int(str(num)[-1])]
##            print(num, current, '70-80')

##        print(num, total)

    print(total)
