import timeit



def fib(number=50):
    total = 0
    a = 0
    b = 1
    if number == 1 or number == 2:
        return 1
    for i in range(1, number):
        c = a + b
        a = b
        b = c

        if c > 4000000:
            return total
        if c % 2 == 0:
            total += c

if __name__ == '__main__':
    print(timeit.timeit(fib, number=50))

        
    # Answer is 4613732
