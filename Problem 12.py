from functools import reduce


number = 0
start = 1
factors = []


while len(factors) < 500:
    number = number + start # generate triangle numbers
    factors = []
    factors = set(reduce(list.__add__, ([[x, number//x] for x in range(1, int(number**0.5) + 1) if number % x == 0])))
    print(number, len(factors))        
    start += 1

print(number)

# Found a much much much faster method here:
# https://stackoverflow.com/questions/6800193/what-is-the-most-efficient-way-of-finding-all-the-factors-of-a-number-in-python


# Biggest speed-ups came from only checking up to the sqrt of the number.
# Then, to get the second factor of the pair, divide the original number by the identified factor.

# list.__add__ adds the two-factor list into the greater list. (list of lists)
# Reduce takes the list of two factors and merges them into the greater list
# Set prevents duplicates




# Answer is 76576500 with 576 factors
